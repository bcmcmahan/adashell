from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'euclid is a pooper'
socketio = SocketIO(app, logger=False)

@app.route('/')
def index():
    return render_template('console.html')



class DirStruct:
    def __init__(self, name="/", parent=None):
        self.name = name
        self.contents = {}
        self.parent = parent

    def add_content(self, folderobj=None,fileobj=None):
        if folderobj:
            self.contents[folderobj.name] = folderobj
        elif fileobj:
            self.contents[fileobj.name] = fileobj

    def show_contents(self):
        def _format(child):
            if isinstance(child, DirStruct):
                return "+---<span class='child_folder'>{}</span>".format(child.name)
            else:
                return "+---<span class='child_file'>{}</span>".format(child.name)

        sorted_contents = sorted(self.contents.values(),
                                 key=lambda x: (1,x.name) if isinstance(x, DirStruct) else (2, x.name))

        cur_name = "Directory listing for <span class='folder'>{}</span>".format(self.name)
        contents = "<br>".join([_format(c) for c in sorted_contents])

        return cur_name + "<br>" + contents

class FileStruct:
    def __init__(self, name="Untitled", content=""):
        self.name = name
        self.contents = content


adafs = DirStruct()
adafs.add_content(fileobj=FileStruct("README", "Hello. Welcome to my experimental shell. Stay tuned for more stuff."))
adafs.add_content(fileobj=FileStruct("DummyFile1", ""))
adafs.add_content(fileobj=FileStruct("DummyFile2", ""))
adafs.add_content(folderobj=DirStruct("data", parent=adafs))
cur_dir = adafs


@socketio.on('command', namespace='/adashell')
def parse_command(message):
    print(message)
    print("parsing command: {}".format(message['content']))
    known_commands = {'su':su, 'ls':ls , 'cd':cd, 'help':help_func,
                      'clear': clear_console, 'less':less, 'echo': echo}
    command = message['content'].split(" ")
    command, args = command[0], command[1:]
    if command not in known_commands:
        emit('print_error', {'info': 'Error: Unknown Command "<b>{}</b>"'.format(command)})
    else:
        func = known_commands[command]
        func(args)

@socketio.on('connection_please', namespace='/adashell')
def connect_now(message):
    print('connecting?')
    emit('connected', {});

def su(args):
    if len(args) == 1:
        if "@" in args[0]:
            args[0] = args[0].split("@")[0]
        emit('auth_success', {'username':args[0]})
    else:
        emit('print_error', {'info': 'Error: Bad Arguments (su expects one)'})

def ls(args):
    emit('print_message', {'info': cur_dir.show_contents()})

def cd(args):
    pass

def help_func(args):
    help_list = ["su - switch user", "ls - list directory contents",
                 "cd - change directory (TODO)", "help - list commands",
                 "less - display file contents", "echo- echo something to shell",
                 "clear - clears the console"]
    emit('print_message', {'info': "<b>Current Commands:</b><br>"+"<br>".join(help_list)})

def less(args):
    if len(args) == 0:
        emit('print_error', {'info': 'Error: Bad Arguments'})
    elif args[0] in cur_dir.contents and isinstance(cur_dir.contents[args[0]], FileStruct):
        emit('print_message', {'info': cur_dir.contents[args[0]].contents})
    else:
        emit('print_error', {'info': 'Error: Unkonwn file "<b>{}</b>"'.format(args[0])})

def echo(args):
    if len(args) == 0:
        emit('print_error', {'info': 'Error: Bad Arguments'})
    else:
        output = " ".join(str(x) for x in args)
        emit("print_message", {'info': output})

def clear_console(args):
    emit('clear_console', {})


if __name__ == '__main__':
    socketio.run(app)
