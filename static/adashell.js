namespace = '/adashell'; // change to an empty string to use the global namespace

    // the socket.io documentation recommends sending an explicit package upon connection
    // this is specially important when using the global namespace
var socket = io.connect('ws://' + document.domain + namespace);


send_message = function(event) {
    socket.emit('command', {content: $('#command_input').val()});
}

connect_to_shell = function() {
    socket.emit('connection_please', {});
}

print_error = function(text) {
    $('#message_container').append("<li><span class='error' id='message_content'>"+text+"</span></li>");
    $('#message_container').scrollTop($('#message_container')[0].scrollHeight);
}

print_msg = function(text) {
    $('#message_container').append("<li><span id='message_content'>"+text+"</span></li>");
    $('#message_container').scrollTop($('#message_container')[0].scrollHeight);
}

socket.on('connected', function(msg) {
    print_msg('Connection established');
}

socket.on('auth_success', function(msg) {
    $('#username').text(msg['username']+'@ada.shell')
});

socket.on('auth_failure', function(msg) {
    print_error(msg['info']);
});

socket.on('nav_success', function(msg) {
    $('#location').text(msg['new_location'])
});

socket.on('nav_failure', function(msg) {
    print_error(msg['info']);
});

socket.on('print_message', function(msg) {
    print_msg(msg['info']);
});

socket.on('print_error', function(msg) {
    print_error(msg['info'])
});

socket.on('clear_console', function(msg) {
    $('#message_container').html("");
})
